from odoo import http
from odoo.http import request
from odoo.addons.web.controllers.main import Home

class Academy(http.Controller) :

    # @http.route('/home/', auth = 'public', website=True)
    # def index(self, **kw):
    #     return request.render('school.index',{'students': request.env['student.student'].search([])})

    @http.route('/student/<int:rec_id>', type='http', auth="public", website=True)
    def Student(self,rec_id, **kw):
        print('idddddddddddddddddddddd',rec_id)
        return request.render('school.student_details',{'student': request.env['student.student'].search([('id','=',rec_id)])})

    @http.route('/register/', auth = 'user', website = True)
    def register(self, **kw) :
        return request.render('school.register')

    @http.route('/register/student/', auth = 'user', website = True)
    def student_register(self, **kw) :
        return request.render('school.student_register')

    @http.route('/products', auth = 'user', website = False)
    def products(self, **kw) :
        Products = request.env['product.product'].sudo().search([], limit = 10)
        return request.render('school.products_listing', {'products' : Products})

    # products
    @http.route('/get_products', type = 'json', auth = "public", csrf = False, website = True)
    def get_products(self, **post) :
        print('innnnnnnnnn')
        Products = request.env['product.product'].sudo().search([],limit=10)
        return {'products' : Products}


class Home(Home) :

    @http.route()
    def index(self, *args, **kw) :
        print('innnnnnnnnnnnnnnnnnnnn')
        if request.session.uid and not request.env['res.users'].sudo().browse(request.session.uid).has_group(
                'base.group_user') :
            return http.local_('/my', query = request.params, keep_hash = True)
        return super(Home, self).index(*args, **kw)

