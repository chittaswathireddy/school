{
    'name' : 'XF Website',
    'version' : '1.0',
    'summary': 'Test Website Module for XF',
    'sequence': 1,
    'description':'''
    Test Website Module.
    ''',

    'category': 'Website',
    'website': 'https://www.xf.in/',
    'author': 'Xfactory.in',
    'depends' : ['base','web','auth_signup'],
    'data': [
        'views/student_details_view.xml',
        'views/template.xml',
        'views/web_body.xml',
        'views/header_footer.xml',
    ],
    'demo': [],
    'qweb': [

        ],
    'installable': True,
    'application': False,
    'auto_install': False,
}
