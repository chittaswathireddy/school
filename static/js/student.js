odoo.define('school.custom_scripts', function(require){
    'use strict';

    var core = require('web.core');
    var QWeb = core.qweb;
    var rpc = require('web.rpc');

    $(document).ready(function(){
        $('#registerStudent').on('click', function(){
//            on register button click, fetches form data and creates a record
            var StudentName = $('#name')[0].value;
            var StudentMobile = $('#mobile')[0].value;
            var StudentAddress = $('#address')[0].value;

            rpc.query({
                model: 'student.student',
                method: 'create',
                args: [{'name': StudentName,
                        'mobile': StudentMobile,
                        'address': StudentAddress}],
            }).then(function (data) {
                console.log('11111111111111111',data);
            });

//            var x = QWeb.render('school.student_details', {  'name': StudentName,
//                                                                'mobile':StudentMobile,
//                                                                'address':StudentAddress });



        })
    });
});




    $(document).ready(function(){
        var listItem = "<li><a href='#'><div class='img-container'><img src='https://xfassets.azureedge.net/media/catalog/category/resized/193x111/Cement-_-RMC.jpg' alt='img' /></div><div class='title-container'>Cement</div></a></li>";
        for(i=1; i<=10; i++){
            console.log(i);
            $(".construction-items ol.products-container").append(listItem);
        }

        for(i=1; i<=5; i++){
            console.log(i);
            $(".industrial-items ol.products-container").append(listItem);
        }

        for(i=1; i<=8; i++){
            console.log(i);
            $(".all-Categories-items ol.products-container").append(listItem);
        }

        // Header script start

        $(".industrial-menu-items-container").hide();

        $('.construction-menu-items-container .menu-dropdown-category-construction').click(function(){
        	$(".construction-menu-items-container .category-dropdown").slideToggle();
        });

        $(".construction-menu-items-container .menu-dropdown-category-industrial").click(function(){
            $(".construction-menu-items-container").hide();
            $(".industrial-menu-items-container .category-dropdown").hide();
            $(".industrial-menu-items-container").show();
            $(".industrial-menu-items-container .menu-dropdown-category-industrial").addClass("selected");
            $(".construction-menu-items-container .menu-dropdown-category-construction").removeClass("selected");

        });

        $('.industrial-menu-items-container .menu-dropdown-category-industrial').click(function(){
            $(".industrial-menu-items-container .category-dropdown").slideToggle();
        });

        $(".industrial-menu-items-container .menu-dropdown-category-construction").click(function(){
         	$(".industrial-menu-items-container").hide();
         	$(".construction-menu-items-container .category-dropdown").hide();
         	$(".construction-menu-items-container").show();
         	$(".construction-menu-items-container .menu-dropdown-category-construction").addClass("selected");
         	$(".industrial-menu-items-container .menu-dropdown-category-industrial").removeClass("selected");
        });

        $(".custom-header .nav-item.dropdown").hover(
                function() {
                    $('.custom-header .dropdown-menu', this).not('.custom-header .in .dropdown-menu').stop(true,true);
                    $(this).toggleClass('open');
                }
        );




            $('.home-banner').slick({
                arrows: true,
                fade: true,
                dots: true,
                autoplay: true
              });



    });