from odoo import api, fields, models, _

class StudentDetails(models.Model):
    _name = "student.student"

    name = fields.Char(string='Name', required=True)
    age = fields.Integer(string='Age')
    standard = fields.Selection([(1,1),(2,2),(3,3),(4,4),(5,5),(6,6),(7,7),(8,8),(9,9),(10,10)], string="Standard")
    address = fields.Text(string="Address")
    mobile = fields.Char(string="Mobile")


